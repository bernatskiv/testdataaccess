Intent:
	Store description of filter for Repository (object for acees to database, see Repository pattern) in implementation agnostic way.
Core Principles:
	1. There is single filter-object for every entity 
	2. That filter-object holds declarative description of filtration that should be applied to storage of that entity
	3. Filter-object has a property for every field of the entity with the same.
	4. Property of filter-object contains description of filtration that could be done using that field
	5. In the implementation of repository filtering of entities should be done basing on descripton of filter-object

Solution structure:
	Library:
		DeclarativeDataAccess //definition of basing filter descriptions
		DeclarativeDataAccess.EntityFramework //implementations of creating filters for EF basing on filter description
		DeclarativeDataAccess.Mongo //implementations of creating filters for Mongo basing on filter description
	ExampleApp: //example of using of Library
		TestDataAccess.APP
		TestDataAccess.DI
		TestDataAccess.DataAccess.Contract //definition of repositories and corresponding filter-descriptions
		TestDataAccess.Domain.Contract //defiinition of services and entities
		TestDataAccess.Domain //implementation of domain cotract
		DataAccessImplementations: //Two variants of implementations to show that it work for both DB-s
			TestDataAccess.DataAccess.EF
			TestDataAccess.DataAccess.Mongo