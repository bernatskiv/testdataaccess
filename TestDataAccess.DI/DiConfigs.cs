﻿using System;

namespace TestDataAccess.DI
{
    public static class DiConfigs
    {
        public static IServiceProvider InitMongoConfig()
        {
            return new MongoDiConfigurator().InitializeDependencies();
        }

        public static IServiceProvider InitEFConfig()
        {
            return new EfDiConfigurator().InitializeDependencies();
        }
    }
}
