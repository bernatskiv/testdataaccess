﻿using Microsoft.Extensions.DependencyInjection;
using System;
using TestExample.Domain;
using TestExample.Domain.Contract.Services;
using TestExample.Domain.Services;

namespace TestDataAccess.DI
{
    public abstract class BaseDiConfigurator
    {
        public void RegisterBusinessLayer(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IDateProvider, DateProvider>();
            serviceCollection.AddTransient<IUserService, UserService>();
        }

        public abstract void RegisterDataAccess(IServiceCollection serviceCollection);

        public void RegisterAll(IServiceCollection serviceCollection)
        {
            RegisterBusinessLayer(serviceCollection);
            RegisterDataAccess(serviceCollection);
        }

        public IServiceProvider InitializeDependencies()
        {
            var serviceCollection = new ServiceCollection();
            RegisterAll(serviceCollection);
            return serviceCollection.BuildServiceProvider();
        }
    }
}
