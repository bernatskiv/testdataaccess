﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using TestExample.DataAccess.Contract.Repositories;
using TestExample.DataAccess.EF;
using TestExample.DataAccess.EF.Repositories;

namespace TestDataAccess.DI
{
    public class EfDiConfigurator : BaseDiConfigurator
    {
        public override void RegisterDataAccess(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<TestExampleDbContext>();
            serviceCollection.AddSingleton<IObjectMapper, ObjectMapper>();
            serviceCollection.AddScoped<IUserRepository, UserRepository>();
        }
    }
}
