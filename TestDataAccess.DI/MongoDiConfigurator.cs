﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using TestExample.DataAccess.Contract.Repositories;
using TestExample.DataAccess.Mongo;

namespace TestDataAccess.DI
{
    public class MongoDiConfigurator : BaseDiConfigurator
    {
        public override void RegisterDataAccess(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<TestExampleDbContext>();
            serviceCollection.AddScoped<IObjectMapper, ObjectMapper>();
            serviceCollection.AddScoped<IUserRepository, UserRepository>();
        }
    }
}
