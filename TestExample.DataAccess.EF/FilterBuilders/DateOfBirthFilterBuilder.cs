﻿using DeclarativeDataAccess.Contract.FilterModels;
using DeclarativeDataAccess.EntityFramework.FilterBuilders;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TestExample.DataAccess.Contract.Repositories;

namespace TestExample.DataAccess.EF.FilterBuilders
{
    public static class DateOfBirthFilterBuilder
    {
        public static Expression<Func<TModel, bool>> ToDbFilter<TModel>(
            this DateOfBirthFilter filterProperty,
            Expression<Func<TModel, DateTime?>> modelProperty)
        {
            switch (filterProperty.Type)
            {
                case DateOfBirthFilter.DobFilterType.Interval:
                    return ValueInIntervalBuilder.ToDbFilter(filterProperty.Interval, GetValueFromNullable(modelProperty));
                case DateOfBirthFilter.DobFilterType.Nullable:
                    return NullableEqualityBuilder.ToDbFilter(filterProperty.Nullable, modelProperty);
                default:
                    ///unfortunately C# requires default even if all cases are listed in switch
                    throw new NotImplementedException();
            }
        }

        public static Expression<Func<TModel, bool>> ToDbFilter<TModel>(
            this CompositeFilter<DateOfBirthFilter> filterProperty,
            Expression<Func<TModel, DateTime?>> modelProperty)
        {
            return CompositeFilterBuilder.ToDbFilter(modelProperty, filterProperty,
                filter => filter.ToDbFilter(modelProperty));
        }

        private static Expression<Func<TModel, DateTime>> GetValueFromNullable<TModel>(Expression<Func<TModel, DateTime?>> expression)
        {
            return ExpressionHelper.UpdateBody<TModel, DateTime?, DateTime>(
                expression, 
                (__, property) => Expression.Property(property, "Value"));
        }
    }
}
