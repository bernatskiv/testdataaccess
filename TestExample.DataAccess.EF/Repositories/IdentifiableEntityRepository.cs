﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TestExample.DataAccess.Contract.Repositories;
using Z.EntityFramework.Plus;

namespace TestExample.DataAccess.EF.Repositories
{
    public abstract class IdentifiableEntityRepository<TEntity, TID, TFilter, TDbModel>
      : BaseRepository<TEntity, TFilter, TDbModel>,
          IIdentfiableEntityRepository<TEntity, TID>
        where TDbModel : class
    {
        protected IdentifiableEntityRepository(TestExampleDbContext dbContext, DbSet<TDbModel> collection, IObjectMapper mapper)
            : base(dbContext, collection, mapper)
        {
        }

        public TEntity GetById(TID id)
        {
            return ToEntity(Collection.Single(GetEqualExpression(id)));
        }

        public void RemoveById(TID id)
        {
            Collection.Where(GetEqualExpression(id)).Delete();
            SaveChanges();
        }

        public void Update(TEntity entity)
        {
            Collection.Update(ToDbModel(entity));
            SaveChanges();
        }

        //TODO: refactore to use simple property expression
        protected abstract Expression<Func<TDbModel, bool>> GetEqualExpression(TID id);
        protected abstract TID GetId(TEntity entity);
    }
}
