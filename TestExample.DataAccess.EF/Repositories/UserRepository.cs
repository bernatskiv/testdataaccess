﻿using DeclarativeDataAccess.EntityFramework;
using DeclarativeDataAccess.EntityFramework.FilterBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TestExample.DataAccess.Contract.Repositories;
using TestExample.DataAccess.EF.DbModels;
using TestExample.DataAccess.EF.FilterBuilders;
using TestExample.Domain.Contract.Entities;

namespace TestExample.DataAccess.EF.Repositories
{
    public class UserRepository
        : IdentifiableEntityRepository<UserEntity, int, UserFilter, UserDbModel>,
          IUserRepository
    {

        public UserRepository(TestExampleDbContext dbContext, IObjectMapper mapper)
            : base(dbContext, dbContext.Users, mapper)
        {
        }

        protected override void SetupFilterMapper(FilterMapper<UserFilter, UserDbModel> filterMapper)
        {
            filterMapper
                .AddMapping(filter => filter.FirstName, property => property.ToDbFilter<UserDbModel>(m => m.FirstName))
                .AddMapping(filter => filter.LastName, property => property.ToDbFilter<UserDbModel>(m => m.LastName))
                .AddMapping(filter => filter.Id, property => property.ToDbFilter<UserDbModel, int>(m => m.Id))
                .AddMapping(filter => filter.Gender, property => property.ToDbFilter<UserDbModel, Gender>(m => m.Gender))
                .AddMapping(filter => filter.DateOrBirth, property => property.ToDbFilter<UserDbModel>(m => m.DateOrBirth))
                .AddMapping(filter => filter.Height, property => property.ToDbFilter<UserDbModel, int>(m => m.Height));
        }

        protected override Expression<Func<UserDbModel, bool>> GetEqualExpression(int id)
        {
            return user => user.Id == id;
        }

        protected override int GetId(UserEntity entity)
        {
            return entity.Id;
        }
    }
}
