﻿using DeclarativeDataAccess.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TestExample.DataAccess.Contract.Repositories;

namespace TestExample.DataAccess.EF.Repositories
{
    public abstract class BaseRepository<TEntity, TFilter, TDbModel> : IBaseRepository<TEntity, TFilter>, IDisposable
        where TDbModel : class
    {
        protected TestExampleDbContext DbContext { get; }

        protected DbSet<TDbModel> Collection { get; }

        protected IObjectMapper Mapper { get; }

        private FilterMapper<TFilter, TDbModel> filterMapper = new FilterMapper<TFilter, TDbModel>();

        protected BaseRepository(TestExampleDbContext dbContext, DbSet<TDbModel> collection, IObjectMapper mapper)
        {
            DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            Collection = collection ?? throw new ArgumentNullException(nameof(collection));
            Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            SetupFilterMapper(filterMapper);
        }

        public void Create(TEntity model)
        {
            Collection.Add(ToDbModel(model));
            SaveChanges();
        }

        public IEnumerable<TEntity> GetByFilter(TFilter filter)
        {
            return Collection.Where(BuildFilter(filter)).Select(ToEntity);
        }

        protected TDbModel ToDbModel(TEntity entity) => Mapper.Map<TEntity, TDbModel>(entity);
        protected TEntity ToEntity(TDbModel dbModel) => Mapper.Map<TDbModel, TEntity>(dbModel);

        private Expression<Func<TDbModel, bool>> BuildFilter(TFilter filter)
        {
            return filterMapper.Map(filter);
        }

        protected abstract void SetupFilterMapper(FilterMapper<TFilter, TDbModel> filterMapper);

        protected void SaveChanges()
        {
            DbContext.SaveChanges();
        }

        #region Dispose
        public void Dispose()
        {
            if (!isDisposed)
            {
                SaveChanges();
                GC.SuppressFinalize(this);
                isDisposed = true;
            }
            
        }

        private volatile bool isDisposed = false;

        ~BaseRepository()
        {
            this.Dispose();
        }
        #endregion
    }
}
