﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TestExample.DataAccess.EF.DbModels;

namespace TestExample.DataAccess.EF
{
    public class TestExampleDbContext : DbContext
    {
        public DbSet<UserDbModel> Users { get; set; }

        public TestExampleDbContext(DbContextOptions options) : base(options)
        {
        }

        public TestExampleDbContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=TextExample;Trusted_Connection=True;");
        }
    }
}
