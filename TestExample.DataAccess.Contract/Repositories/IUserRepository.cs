﻿using DeclarativeDataAccess.Contract.FilterModels;
using DeclarativeDataAccess.FilterModels;
using System;
using System.Collections.Generic;
using System.Text;
using TestExample.Domain.Contract.Entities;

namespace TestExample.DataAccess.Contract.Repositories
{
    public interface IUserRepository : 
        IBaseRepository<UserEntity, UserFilter>, 
        IIdentfiableEntityRepository<UserEntity, int>
    {
    }

    /// <summary>
    /// This class is used to filter users in IUserRepository
    /// Every filter property is used to filter by corresponding property (it has the same name)
    /// If filter property contains null then it's not applied
    /// </summary>
    public class UserFilter
    {
        public AnyOf<int> Id { get; set; }
        public ValueInInterval<int> Height { get; set; }
        public StringSearchParameter FirstName { get; set; }
        public StringSearchParameter LastName { get; set; }
        public CompositeFilter<DateOfBirthFilter> DateOrBirth { get; set; }
        public NonNullableEquality<Gender> Gender { get; set; }
    }

    /// <summary>
    /// Just single property has value. It's analogue of discriminated union in F#.
    /// So DobFilter contains one of the subfilters.
    /// See: IDiscriminatedUnion
    /// </summary>
    public class DateOfBirthFilter : IDiscriminatedUnion
    {
        public enum DobFilterType
        {
            Nullable = 0,
            Interval
        }

        public DobFilterType Type { get; private set; }

        public NullableEquality<DateTime?> Nullable { get; private set; }
        public ValueInInterval<DateTime> Interval { get; private set; }

        public static DateOfBirthFilter InitNullable(NullableEquality<DateTime?> value) => new DateOfBirthFilter { Nullable = value, Type = DobFilterType.Nullable };
        public static DateOfBirthFilter InitInterval(ValueInInterval<DateTime> value) => new DateOfBirthFilter { Interval = value, Type = DobFilterType.Interval };

        private DateOfBirthFilter() { }
    }
}
