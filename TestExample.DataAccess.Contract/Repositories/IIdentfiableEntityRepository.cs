﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestExample.DataAccess.Contract.Repositories
{
    public interface IIdentfiableEntityRepository<TEntity, TID>
    {
        TEntity GetById(TID id);

        void RemoveById(TID id);

        void Update(TEntity model);
    }
}
