﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestExample.DataAccess.Contract.Repositories
{
    public interface IBaseRepository<TEntity, TFilter>
    {
        void Create(TEntity model);

        IEnumerable<TEntity> GetByFilter(TFilter filter);
    }
}
