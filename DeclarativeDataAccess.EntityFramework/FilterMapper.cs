﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Linq;
using DeclarativeDataAccess.EntityFramework.FilterBuilders;

namespace DeclarativeDataAccess.EntityFramework
{
    public class FilterMapper<TFilter, TModel>
    {
        /// <summary>
        /// Expression<Func<TModel, bool>> - this is the type of DbFilter. 
        /// So all mappings goes from TFilter to this Expression type
        /// </summary>
        private List<Func<TFilter, Expression<Func<TModel, bool>>>> mappers = 
            new List<Func<TFilter, Expression<Func<TModel, bool>>>>();

        public Expression<Func<TModel, bool>> Map(TFilter filter)
        {
            var dbFilters = mappers.Select(map => map(filter))
                                   .Where(dbFilter => dbFilter != null)
                                   .ToArray();
            return dbFilters.Any()
                ? CompositeFilterBuilder.And(dbFilters)
                : BuildEmptyFilter();
        }

        public FilterMapper<TFilter, TModel> AddMapping<TFilterProperty>(
            Func<TFilter, TFilterProperty> filterPropertySelector,
            Func<TFilterProperty, Expression<Func<TModel, bool>>> propretyToDbFilterMapper)
        {
            if (filterPropertySelector == null) throw new ArgumentNullException(nameof(filterPropertySelector));
            if (propretyToDbFilterMapper == null) throw new ArgumentNullException(nameof(propretyToDbFilterMapper));
            mappers.Add(filter => MapProperty(filterPropertySelector, propretyToDbFilterMapper, filter));
            return this;
        }

        private Expression<Func<TModel, bool>> MapProperty<TFilterProperty>(
            Func<TFilter, TFilterProperty> filterPropertySelector,
            Func<TFilterProperty, Expression<Func<TModel, bool>>> propretyToFilterMapper,
            TFilter filter)
        {
            var filterProperty = filterPropertySelector(filter);
            return filterProperty != null ? propretyToFilterMapper(filterProperty) : null;
        }

        private static Expression<Func<TModel, bool>> BuildEmptyFilter()
        {
            return Expression.Lambda<Func<TModel, bool>>(Expression.Constant(true), Expression.Parameter(typeof(TModel)));
        }
    }
}
