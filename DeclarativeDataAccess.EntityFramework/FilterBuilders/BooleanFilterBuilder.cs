﻿using DeclarativeDataAccess.Contract.FilterModels;
using DeclarativeDataAccess.FilterModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DeclarativeDataAccess.EntityFramework.FilterBuilders
{
    public static class BooleanFilterBuilder
    {
        public static Expression<Func<TModel, bool>> ToDbFilter<TModel, TFilter>(
            this TFilter filterProperty,
            Func<TFilter, Expression<Func<TModel, bool>>> filterGenerator)
            where TFilter : BooleanFilter
        {
            var dbFilter = filterGenerator(filterProperty);
            if (filterProperty.Inverted)
            {
                var (parameter, body) = ExpressionHelper.Split<TModel, bool>(dbFilter);
                var newBody = Expression.Not(body);
                dbFilter = Expression.Lambda<Func<TModel, bool>>(newBody, parameter);
            }

            return dbFilter;
        }
    }
}
