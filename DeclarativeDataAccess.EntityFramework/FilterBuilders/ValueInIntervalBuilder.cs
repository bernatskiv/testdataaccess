﻿using DeclarativeDataAccess.Contract.FilterModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DeclarativeDataAccess.EntityFramework.FilterBuilders
{
    public static class ValueInIntervalBuilder
    {
        public static Expression<Func<TModel, bool>> ToDbFilter<TModel, TProperty>(
            this ValueInInterval<TProperty> filterProperty,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty,
                __ => CompositeFilterBuilder.And(BuildIntervalProperty(modelProperty, filterProperty)));
        }

        private static IEnumerable<Expression<Func<TModel, bool>>> BuildIntervalProperty<TModel, TProperty>(
            Expression<Func<TModel, TProperty>> modelProperty,
            ValueInInterval<TProperty> filterProperty)
        {
            if (filterProperty.IsExactValue)
            {
                yield return ExpressionHelper.UpdateBody(modelProperty,
                    (parameter, property) => Expression.Equal(property, Expression.Constant(filterProperty.Min.Value)));
            }
            else
            {
                var minBorder = filterProperty.Min;
                switch (minBorder.Type)
                {
                    case IntervalEndingType.NoBorder:
                        break;
                    case IntervalEndingType.Including:
                        yield return ExpressionHelper.UpdateBody(modelProperty,
                            (parameter, property) => Expression.GreaterThanOrEqual(property, Expression.Constant(minBorder.Value)));
                        break;
                    case IntervalEndingType.Excluding:
                        yield return ExpressionHelper.UpdateBody(modelProperty,
                            (parameter, property) => Expression.GreaterThan(property, Expression.Constant(minBorder.Value)));
                        break;
                    default:
                        throw new NotImplementedException($"Doesn't implemented case of {minBorder.Type}");
                };

                var maxBorder = filterProperty.Max;
                switch (maxBorder.Type)
                {
                    case IntervalEndingType.NoBorder:
                        break;
                    case IntervalEndingType.Including:
                        yield return ExpressionHelper.UpdateBody(modelProperty,
                            (parameter, property) => Expression.LessThanOrEqual(property, Expression.Constant(maxBorder.Value)));
                        break;
                    case IntervalEndingType.Excluding:
                        yield return ExpressionHelper.UpdateBody(modelProperty,
                            (parameter, property) => Expression.LessThan(property, Expression.Constant(maxBorder.Value)));
                        break;
                    default:
                        throw new NotImplementedException($"Doesn't implemented case of {maxBorder.Type}");
                };
            }
        }
    }
}
