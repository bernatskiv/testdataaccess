﻿using DeclarativeDataAccess.Contract.FilterModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DeclarativeDataAccess.EntityFramework.FilterBuilders
{
    public static class NullableEqualityBuilder
    {
        public static Expression<Func<TModel, bool>> ToDbFilter<TModel, TProperty>(
            this NullableEquality<TProperty> filterProperty,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return BooleanFilterBuilder.ToDbFilter(filterProperty,
                __ => filterProperty.HasValue
                        ? ToDbFilterHasValue(filterProperty, modelProperty)
                        : ToDbFilterNoValue(filterProperty, modelProperty));
        }

        private static Expression<Func<TModel, bool>> ToDbFilterHasValue<TModel, TProperty>(
            this NullableEquality<TProperty> filterProperty,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return ExpressionHelper.UpdateBody(
                modelProperty,
                (parameter, property) => Expression.Equal(property, Expression.Constant(filterProperty.Value)));
        }

        private static Expression<Func<TModel, bool>> ToDbFilterNoValue<TModel, TProperty>(
            this NullableEquality<TProperty> filterProperty,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return ExpressionHelper.UpdateBody(
                modelProperty,
                (parameter, property) => Expression.Equal(property, Expression.Constant(null)));
        }
    }
}
