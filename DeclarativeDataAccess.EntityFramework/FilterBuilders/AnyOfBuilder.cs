﻿using DeclarativeDataAccess.Contract.FilterModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Linq;
using System.Reflection;

namespace DeclarativeDataAccess.EntityFramework.FilterBuilders
{
    public static class AnyOfBuilder
    {
        public static Expression<Func<TModel, bool>> ToDbFilter<TModel, TProperty>(
            this AnyOf<TProperty> filterProperty,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty, 
                __ => filterProperty.ToDbFilterInternal(modelProperty));
        }

        private static Expression<Func<TModel, bool>> ToDbFilterInternal<TModel, TProperty>(
            this AnyOf<TProperty> filterProperty,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            var containsMethod = ExtractMethodInfo(GetContainsExpression(filterProperty.Values));

            return ExpressionHelper.UpdateBody(
                modelProperty,
                (parameter, property) => Expression.Call(containsMethod, Expression.Constant(filterProperty.Values), property));
        }

        private static Expression<Func<TProperty, bool>> GetContainsExpression<TProperty>(IEnumerable<TProperty> collection)
        {
            return item => collection.Contains(item);
        }

        private static MethodInfo ExtractMethodInfo<TProperty>(Expression<Func<TProperty, bool>> methodCallExpression)
        {
            return ((MethodCallExpression)methodCallExpression.Body).Method;
        }
    }
}
