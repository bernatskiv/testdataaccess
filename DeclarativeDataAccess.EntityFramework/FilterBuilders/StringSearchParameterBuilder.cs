﻿using DeclarativeDataAccess.Contract.FilterModels;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace DeclarativeDataAccess.EntityFramework.FilterBuilders
{
    public static class StringSearchParameterBuilder
    {
        public static Expression<Func<TModel, bool>> ToDbFilter<TModel>(
            this StringSearchParameter filterProperty,
            Expression<Func<TModel, string>> modelProperty)
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty,
                __ =>
                {
                    MethodInfo method;
                    switch(filterProperty.MatchType)
                    {
                        case StringSearchType.FullMatch:
                            method = equalsMethod;
                            break;
                        case StringSearchType.SubStringMatch:
                            method = containsMethod;
                            break;
                        default:
                            throw new NotImplementedException();
                    }

                    return ExpressionHelper.UpdateBody(modelProperty, 
                        (parameter, property) => Expression.Call(property, method, Expression.Constant(filterProperty.String)));
                });
        }

        private static MethodInfo containsMethod = typeof(string).GetMethod(nameof(string.Contains), new[] { typeof(string) });
        private static MethodInfo equalsMethod = typeof(string).GetMethod(nameof(string.Equals), new[] { typeof(string) });
    }
}
