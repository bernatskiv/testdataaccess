﻿using DeclarativeDataAccess.Contract.FilterModels;
using DeclarativeDataAccess.EntityFramework.FilterBuilders;
using DeclarativeDataAccess.FilterModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DeclarativeDataAccess.EntityFramework.FilterBuilders
{
    public static class NonNullableEqualityBuilder
    {
        public static Expression<Func<TModel, bool>> ToDbFilter<TModel, TProperty>(
            this NonNullableEquality<TProperty> filterProperty,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty,
                __ => ToDbFilterInternal(filterProperty, modelProperty));
        }

        private static Expression<Func<TModel, bool>> ToDbFilterInternal<TModel, TProperty>(
            this NonNullableEquality<TProperty> filterProperty,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return ExpressionHelper.UpdateBody(
                modelProperty, 
                (parameter, property) => Expression.Equal(property, Expression.Constant(filterProperty.Value)));
        }
    }
}
