﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DeclarativeDataAccess.EntityFramework.FilterBuilders
{
    public static class ExpressionHelper
    {
        public static (ParameterExpression parameter, Expression body) Split<TInput, TOutput>
            (Expression<Func<TInput, TOutput>> expression)
            => (expression.Parameters.Single(), expression.Body);

        public static Expression<Func<TModel, bool>> UpdateBody<TModel, TProperty>(
            Expression<Func<TModel, TProperty>> expression,
            Func<ParameterExpression, Expression, Expression> bodyProcessor)
        {
            return UpdateBody<TModel, TProperty, bool>(expression, bodyProcessor);
        }

        public static Expression<Func<TModel, TOutput>> UpdateBody<TModel, TProperty, TOutput>(
            Expression<Func<TModel, TProperty>> expression, 
            Func<ParameterExpression, Expression, Expression> bodyProcessor)
        {
            var (parameter, body) = Split(expression);
            var newBody = bodyProcessor(parameter, body);
            return Expression.Lambda<Func<TModel, TOutput>>(newBody, parameter);
        }
    }
}
