﻿using DeclarativeDataAccess.Contract.FilterModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DeclarativeDataAccess.EntityFramework.FilterBuilders
{
    public static class CompositeFilterBuilder
    {
        public static Expression<Func<TModel, bool>> ToDbFilter<TModel, TProperty, TFilter>(
           Expression<Func<TModel, TProperty>> modelProperty,
           CompositeFilter<TFilter> filterProperty,
           Func<TFilter, Expression<Func<TModel, bool>>> toDbFilter)
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty,
                __ => ToDbFilterCore(modelProperty, filterProperty, toDbFilter));
        }

        public static Expression<Func<TModel, bool>> ToDbFilterCore<TModel, TProperty, TFilter>(
           Expression<Func<TModel, TProperty>> modelProperty,
           CompositeFilter<TFilter> filterProperty,
           Func<TFilter, Expression<Func<TModel, bool>>> toDbFilter)
        {
            var simpleFilters = filterProperty.Filters.Select(toDbFilter);
            var complexFilters = filterProperty.CompositeFilters
                .Select(filter => ToDbFilter(modelProperty, filter, toDbFilter));
            var allFilters = simpleFilters.Concat(complexFilters);
            switch (filterProperty.Type)
            {
                case CompositionType.And:
                    return And(allFilters);
                case CompositionType.Or:
                    return Or(allFilters);
                default:
                    throw new NotImplementedException();
            }
        }

        public static Expression<Func<TModel, bool>> And<TModel>(IEnumerable<Expression<Func<TModel, bool>>> expressions)
        {
            return Aggregate(expressions, Expression.And);
        }

        public static Expression<Func<TModel, bool>> Or<TModel>(IEnumerable<Expression<Func<TModel, bool>>> expressions)
        {
            return Aggregate(expressions, Expression.Or);
        }

        private static Expression<Func<TModel, bool>> Aggregate<TModel>(
            IEnumerable<Expression<Func<TModel, bool>>> expressions,
            Func<Expression, Expression, Expression> aggregator)
        {
            var parameter = ExpressionHelper.Split(expressions.First()).parameter;
            var body = expressions
                        .Select(exp => ReplaceParameterVisitor.Replace(exp, parameter))
                        .Select(exp => ExpressionHelper.Split(exp).body)
                        .Aggregate(aggregator);
            return Expression.Lambda<Func<TModel, bool>>(body, parameter);
        }

        private class ReplaceParameterVisitor : ExpressionVisitor
        {
            public static Expression<Func<TInput, TOutput>> Replace<TInput, TOutput>(
                Expression<Func<TInput, TOutput>> expression, 
                ParameterExpression newParameter)
            {
                return (Expression<Func<TInput, TOutput>>)
                    new ReplaceParameterVisitor(expression.Parameters.Single(), newParameter)
                        .Visit(expression);
            }

            private readonly ParameterExpression originalParameter;
            private readonly ParameterExpression newParameter;

            private ReplaceParameterVisitor(ParameterExpression originalParameter, ParameterExpression newParameter)
            {
                this.originalParameter = originalParameter ?? throw new ArgumentNullException(nameof(originalParameter));
                this.newParameter = newParameter ?? throw new ArgumentNullException(nameof(newParameter));
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return node == originalParameter ? newParameter : node;
            }
        }
    }
}
