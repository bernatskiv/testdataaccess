﻿DeclarativeDataAccess.EntityFramework project contains FilterBuilders and FilterMapper.
FilterBuilders are classes that creates Expressions for filtering of DbSet collections in DbContext. These Expressions are created basing on filter descriptions that in DeclarativeDataAccess project.
FilterMapper is used to map filter-object to Expression.