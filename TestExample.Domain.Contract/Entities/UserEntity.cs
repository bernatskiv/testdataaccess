﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestExample.Domain.Contract.Entities
{
    public sealed class UserEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Height { get; set; }
        /// <summary>
        /// Not required property
        /// </summary>
        public DateTime? DateOrBirth { get; set; }
        public Gender Gender { get; set; }
    }

    public enum Gender
    {
        Female = 0,
        Male
    }
}
