﻿using System;
using System.Collections.Generic;
using System.Text;
using TestExample.Domain.Contract.Entities;

namespace TestExample.Domain.Contract.Services
{
    public interface IUserService
    {
        UserEntity GetById(int id);
        void RemoveById(int id);
        void Update(UserEntity entity);
        void Create(UserEntity entity);

        IEnumerable<UserEntity> GetAll();
        IEnumerable<UserEntity> GetTallMenWithName(string name);
        IEnumerable<UserEntity> GetAdultPeople();
        IEnumerable<UserEntity> GetNonAdult(bool includeWithouDob);
        IEnumerable<UserEntity> GetNonMale();

    }    
}
