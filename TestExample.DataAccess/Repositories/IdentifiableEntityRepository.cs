﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TestExample.DataAccess.Contract.Repositories;

namespace TestExample.DataAccess.Mongo.Repositories
{
    public abstract class IdentifiableEntityRepository<TEntity, TID, TFilter, TDbModel>
       : BaseRepository<TEntity, TFilter, TDbModel>,
           IIdentfiableEntityRepository<TEntity, TID>
    {
        protected IdentifiableEntityRepository(IMongoCollection<TDbModel> collection, IObjectMapper mapper)
            : base(collection, mapper)
        {
        }

        public TEntity GetById(TID id)
        {
            return ToEntity(Collection.Find(GetEqualExpression(id)).Single());
        }

        public void RemoveById(TID id)
        {
            Collection.DeleteOne(GetEqualExpression(id));
        }

        public void Update(TEntity entity)
        {
            Collection.ReplaceOne(GetEqualExpression(GetId(entity)), ToDbModel(entity));
        }

        //TODO: refactore to use simple property expression
        protected abstract Expression<Func<TDbModel, bool>> GetEqualExpression(TID id);
        protected abstract TID GetId(TEntity entity);
    }
}
