﻿using DeclarativeDataAccess.Mongo;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestExample.DataAccess.Contract.Repositories;

namespace TestExample.DataAccess.Mongo.Repositories
{
    public abstract class BaseRepository<TEntity, TFilter, TDbModel> : IBaseRepository<TEntity, TFilter>
    {
        protected IMongoCollection<TDbModel> Collection { get; }

        protected IObjectMapper Mapper { get; }

        private FilterMapper<TFilter, TDbModel> filterMapper = new FilterMapper<TFilter, TDbModel>();

        protected BaseRepository(IMongoCollection<TDbModel> collection, IObjectMapper mapper)
        {
            Collection = collection ?? throw new ArgumentNullException(nameof(collection));
            Mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            SetupFilterMapper(filterMapper);
        }

        public void Create(TEntity model)
        {
            Collection.InsertOne(ToDbModel(model));
        }

        public IEnumerable<TEntity> GetByFilter(TFilter filter)
        {
            return Collection.Find(BuildFilter(filter)).ToList().Select(ToEntity);
        }

        protected TDbModel ToDbModel(TEntity entity) => Mapper.Map<TEntity, TDbModel>(entity);
        protected TEntity ToEntity(TDbModel dbModel) => Mapper.Map<TDbModel, TEntity>(dbModel);

        private FilterDefinition<TDbModel> BuildFilter(TFilter filter)
        {
            return filterMapper.Map(filter);
        }

        protected abstract void SetupFilterMapper(FilterMapper<TFilter, TDbModel> filterMapper);
    }
}
