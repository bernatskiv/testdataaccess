﻿using System;
using TestExample.DataAccess.Contract.Repositories;
using TestExample.DataAccess.Mongo.FilterBuilders;
using TestExample.Domain.Contract.Entities;
using DeclarativeDataAccess.Mongo.FilterBuilders;
using TestExample.DataAccess.Mongo.Repositories;
using System.Linq.Expressions;
using DeclarativeDataAccess.Mongo;

namespace TestExample.DataAccess.Mongo
{
    public class UserRepository 
        : IdentifiableEntityRepository<UserEntity, int, UserFilter, UserDbModel>,
          IUserRepository
    {

        public UserRepository(TestExampleDbContext dbContext, IObjectMapper mapper)
            : base(dbContext.Users, mapper)
        {
        }

        protected override void SetupFilterMapper(FilterMapper<UserFilter, UserDbModel> filterMapper)
        {
            filterMapper
                .AddMapping(filter => filter.FirstName, (property, dbFilterBuilder) => property.ToDbFilter(dbFilterBuilder, m => m.FirstName, true))
                .AddMapping(filter => filter.LastName, (property, dbFilterBuilder) => property.ToDbFilter(dbFilterBuilder, m => m.LastName, true))
                .AddMapping(filter => filter.Id, (property, dbFilterBuilder) => property.ToDbFilter(dbFilterBuilder, m => m.Id))
                .AddMapping(filter => filter.Gender, (property, dbFilterBuilder) => property.ToDbFilter(dbFilterBuilder, m => m.Gender))
                .AddMapping(filter => filter.DateOrBirth, (property, dbFilterBuilder) => property.ToDbFilter(dbFilterBuilder, m => m.DateOrBirth))
                .AddMapping(filter => filter.Height, (property, dbFilterBuilder) => property.ToDbFilter(dbFilterBuilder, m => m.Height));
        }

        protected override Expression<Func<UserDbModel, bool>> GetEqualExpression(int id)
        {
            return user => user.Id == id;
        }

        protected override int GetId(UserEntity entity)
        {
            return entity.Id;
        }
    }
}
