﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using TestExample.Domain.Contract.Entities;

namespace TestExample.DataAccess.Mongo
{
    public class TestExampleDbContext
    {
        private readonly IMongoDatabase database;

        public TestExampleDbContext(MongoUrl connectionUrl)
        {
            MongoClient client = new MongoClient(connectionUrl);
            database = client.GetDatabase(connectionUrl.DatabaseName);
        }

        public TestExampleDbContext() 
            : this(new MongoUrl("mongodb://localhost:27017/TestExample") )
        {
        }

        public IMongoCollection<UserDbModel> Users => Collection<UserDbModel>(nameof(Users));

        private IMongoCollection<TItem> Collection<TItem>(string name)
        {
            return database.GetCollection<TItem>(name);
        }
    }
}
