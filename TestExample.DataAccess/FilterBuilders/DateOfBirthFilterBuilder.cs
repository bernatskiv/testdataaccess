﻿using DeclarativeDataAccess.Contract.FilterModels;
using DeclarativeDataAccess.FilterModels;
using DeclarativeDataAccess.Mongo.FilterBuilders;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Linq.Expressions;
using TestExample.DataAccess.Contract.Repositories;

namespace TestExample.DataAccess.Mongo.FilterBuilders
{
    public static class DateOfBirthFilterBuilder
    {
        public static FilterDefinition<TModel> ToDbFilter<TModel>(
            this DateOfBirthFilter filterProperty,
            FilterDefinitionBuilder<TModel> filterBuilder,
            Expression<Func<TModel, DateTime?>> modelProperty)
        {
            switch (filterProperty.Type)
            {
                case DateOfBirthFilter.DobFilterType.Interval:
                    return ValueInIntervalBuilder.ToDbFilter(ToNullableInterval(filterProperty.Interval), filterBuilder, modelProperty);
                case DateOfBirthFilter.DobFilterType.Nullable:
                    return NullableEqualityBuilder.ToDbFilter(filterProperty.Nullable, filterBuilder, modelProperty);
                default:
                    ///unfortunately C# requires default even if all cases are listed in switch
                    throw new NotImplementedException();
            }
            throw new NotImplementedException();
        }

        public static FilterDefinition<TModel> ToDbFilter<TModel>(
            this CompositeFilter<DateOfBirthFilter> filterProperty,
            FilterDefinitionBuilder<TModel> filterBuilder,
            Expression<Func<TModel, DateTime?>> modelProperty)
        {
            return CompositeFilterBuilder.ToDbFilter(filterBuilder, modelProperty, filterProperty, 
                filter => filter.ToDbFilter(filterBuilder, modelProperty));
        }

        private static ValueInInterval<DateTime?> ToNullableInterval(ValueInInterval<DateTime> interval)
        {
            var convertedInterval = ValueInInterval<DateTime?>
                                    .CreateInterval(Convert(interval.Min), Convert(interval.Max));
            if (interval.Inverted)
            {
                convertedInterval = BooleanFilter.Not(convertedInterval);
            }
            return convertedInterval;
        }

        private static IntervalEnding<TStruct?> Convert<TStruct>(IntervalEnding<TStruct> ending)
            where TStruct : struct
        {
            switch (ending.Type)
            {
                case IntervalEndingType.NoBorder:
                    return IntervalEnding<TStruct?>.NoBorder();
                case IntervalEndingType.Excluding:
                    return IntervalEnding<TStruct?>.ExcludingValue(ending.Value);
                case IntervalEndingType.Including:
                    return IntervalEnding<TStruct?>.IncludingValue(ending.Value);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
