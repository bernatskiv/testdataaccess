﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TestExample.Domain.Contract.Entities;

namespace TestExample.DataAccess.Mongo
{
    public interface IObjectMapper
    {
        TOutput Map<TInput, TOutput>(TInput input);
    }

    public class ObjectMapper : IObjectMapper
    {
        private readonly IMapper mapper;

        public ObjectMapper()
        {
            mapper = SetupMapping();
        }
        public TOutput Map<TInput, TOutput>(TInput input) => mapper.Map<TInput, TOutput>(input);

        private IMapper SetupMapping()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserEntity, UserDbModel>();
                cfg.CreateMap<UserDbModel, UserEntity>();
            });

            var mapper = config.CreateMapper();
            return mapper;
        }
    }
}
