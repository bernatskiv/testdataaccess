﻿using System;
using System.Collections.Generic;
using System.Text;
using TestExample.Domain.Contract.Entities;

namespace TestExample.DataAccess.Mongo
{
    public class UserDbModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOrBirth { get; set; }
        public Gender Gender { get; set; }
        public int Height { get; set; }
    }
}
