﻿using DeclarativeDataAccess.Contract.FilterModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DeclarativeDataAccess.Mongo.FilterBuilders
{
    public static class ValueInIntervalBuilder
    {
        public static FilterDefinition<TModel> ToDbFilter<TModel, TProperty>(
            this ValueInInterval<TProperty> filterProperty,
            FilterDefinitionBuilder<TModel> filterBuilder,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty, 
                filterBuilder, 
                __ => filterBuilder.And(BuildIntervalProperty(filterBuilder, modelProperty, filterProperty)));
        }

        private static IEnumerable<FilterDefinition<TModel>> BuildIntervalProperty<TModel, TProperty>(
            FilterDefinitionBuilder<TModel> FilterBuilder,
            Expression<Func<TModel, TProperty>> modelProperty,
            ValueInInterval<TProperty> filterProperty)
        {
            if (filterProperty.IsExactValue)
            {
                yield return FilterBuilder.Eq(modelProperty, filterProperty.Min.Value);
            }
            else
            {
                var minBorder = filterProperty.Min;
                switch (minBorder.Type)
                {
                    case IntervalEndingType.NoBorder:
                        break;
                    case IntervalEndingType.Including:
                        yield return FilterBuilder.Gte(modelProperty, minBorder.Value);
                        break;
                    case IntervalEndingType.Excluding:
                        yield return FilterBuilder.Gt(modelProperty, minBorder.Value);
                        break;
                    default:
                        throw new NotImplementedException($"Doesn't implemented case of {minBorder.Type}");
                };

                var maxBorder = filterProperty.Max;
                switch (maxBorder.Type)
                {
                    case IntervalEndingType.NoBorder:
                        break;
                    case IntervalEndingType.Including:
                        yield return FilterBuilder.Lte(modelProperty, maxBorder.Value);
                        break;
                    case IntervalEndingType.Excluding:
                        yield return FilterBuilder.Lt(modelProperty, maxBorder.Value);
                        break;
                    default:
                        throw new NotImplementedException($"Doesn't implemented case of {maxBorder.Type}");
                };
            }
        }
    }
}
