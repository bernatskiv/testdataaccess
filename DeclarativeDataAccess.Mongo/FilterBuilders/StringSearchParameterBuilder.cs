﻿using DeclarativeDataAccess.Contract.FilterModels;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace DeclarativeDataAccess.Mongo.FilterBuilders
{
    public static class StringSearchParameterBuilder
    {
        public static FilterDefinition<TModel> ToDbFilter<TModel>(
            this StringSearchParameter filterProperty,
            FilterDefinitionBuilder<TModel> filterBuilder,
            Expression<Func<TModel, string>> modelProperty, 
            bool ignoreCase) // in sql this flag depends on column type, so we cannot put it into common library
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty,
                filterBuilder,
                __ =>
                {
                    switch (filterProperty.MatchType)
                    {
                        case StringSearchType.SubStringMatch:
                            return HasSubString(filterBuilder, modelProperty, filterProperty.String, ignoreCase);
                        case StringSearchType.FullMatch:
                            return FullMatch(filterBuilder, modelProperty, filterProperty.String, ignoreCase);
                        default:
                            throw new NotImplementedException();
                    }
                });
        }

        private static FilterDefinition<TModel> FullMatch<TModel>(
            FilterDefinitionBuilder<TModel> filterBuilder,
            Expression<Func<TModel, string>> modelProperty,
            string @string,
            bool ignoreCase)
        {
            if (!ignoreCase)
            {
                return filterBuilder.Eq(modelProperty, @string);
            }

            var escapedString = $"^{Regex.Escape(@string)}$";
            var substringRegex = new Regex(escapedString, RegexOptions.IgnoreCase);
            var propertyName = ((MemberExpression)modelProperty.Body).Member.Name;
            return filterBuilder.Regex(propertyName, substringRegex);
        }

        private static FilterDefinition<TModel> HasSubString<TModel>(
            FilterDefinitionBuilder<TModel> filterBuilder,
            Expression<Func<TModel, string>> modelProperty,
            string @string,
            bool ignoreCase)
        {
            var escapedString = Regex.Escape(@string);
            var substringRegex = new Regex(escapedString, ignoreCase ? RegexOptions.IgnoreCase : RegexOptions.None);
            var propertyName = ((MemberExpression)modelProperty.Body).Member.Name;
            return filterBuilder.Regex(propertyName, substringRegex);
        }
    }
}
