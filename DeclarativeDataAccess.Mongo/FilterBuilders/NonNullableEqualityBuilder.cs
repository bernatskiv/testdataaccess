﻿using DeclarativeDataAccess.Contract.FilterModels;
using DeclarativeDataAccess.FilterModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DeclarativeDataAccess.Mongo.FilterBuilders
{
    public static class NonNullableEqualityBuilder
    {
        public static FilterDefinition<TModel> ToDbFilter<TModel, TProperty>(
            this NonNullableEquality<TProperty> filterProperty,
            FilterDefinitionBuilder<TModel> filterBuilder,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty, 
                filterBuilder, 
                __ => filterBuilder.Eq(modelProperty, filterProperty.Value));
        }
    }
}
