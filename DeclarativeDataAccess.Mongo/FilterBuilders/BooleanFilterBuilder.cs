﻿using DeclarativeDataAccess.Contract.FilterModels;
using DeclarativeDataAccess.FilterModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DeclarativeDataAccess.Mongo.FilterBuilders
{
    public static class BooleanFilterBuilder
    {
        public static FilterDefinition<TModel> ToDbFilter<TModel, TFilter>(
            this TFilter filterProperty,
            FilterDefinitionBuilder<TModel> filterBuilder,
            Func<TFilter, FilterDefinition<TModel>> filterGenerator)
            where TFilter : BooleanFilter
        {
            var dbFilter = filterGenerator(filterProperty);
            if (filterProperty.Inverted)
            {
                dbFilter = filterBuilder.Not(dbFilter);
            }

            return dbFilter;
        }
    }
}
