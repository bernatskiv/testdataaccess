﻿using DeclarativeDataAccess.Contract.FilterModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DeclarativeDataAccess.Mongo.FilterBuilders
{
    public static class CompositeFilterBuilder
    {
        public static FilterDefinition<TModel> ToDbFilter<TModel, TProperty, TFilter>(
           this FilterDefinitionBuilder<TModel> filterBuilder,
           Expression<Func<TModel, TProperty>> modelProperty,
           CompositeFilter<TFilter> filterProperty,
           Func<TFilter, FilterDefinition<TModel>> toDbFilter)
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty, 
                filterBuilder, 
                __ => ToDbFilterCore(filterBuilder, modelProperty, filterProperty, toDbFilter));
        }

        public static FilterDefinition<TModel> ToDbFilterCore<TModel, TProperty, TFilter>(
           this FilterDefinitionBuilder<TModel> filterBuilder,
           Expression<Func<TModel, TProperty>> modelProperty,
           CompositeFilter<TFilter> filterProperty,
           Func<TFilter, FilterDefinition<TModel>> toDbFilter)
        {
            var simpleFilters = filterProperty.Filters.Select(toDbFilter);
            var complexFilters = filterProperty.CompositeFilters
                .Select(filter => ToDbFilter(filterBuilder, modelProperty, filter, toDbFilter));
            var allFilters = simpleFilters.Concat(complexFilters);
            switch (filterProperty.Type)
            {
                case CompositionType.And:
                    return filterBuilder.And(allFilters);
                case CompositionType.Or:
                    return filterBuilder.Or(allFilters);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
