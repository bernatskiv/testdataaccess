﻿using DeclarativeDataAccess.Contract.FilterModels;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DeclarativeDataAccess.Mongo.FilterBuilders
{
    public static class AnyOfBuilder
    {
        public static FilterDefinition<TModel> ToDbFilter<TModel, TProperty>(
            this AnyOf<TProperty> filterProperty,
            FilterDefinitionBuilder<TModel> filterBuilder,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty, 
                filterBuilder, 
                __ => filterBuilder.In(modelProperty, filterProperty.Values));
        }
    }
}
