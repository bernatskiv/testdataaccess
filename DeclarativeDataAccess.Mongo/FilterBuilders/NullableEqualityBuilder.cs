﻿using DeclarativeDataAccess.Contract.FilterModels;
using MongoDB.Driver;
using System;
using System.Linq.Expressions;

namespace DeclarativeDataAccess.Mongo.FilterBuilders
{
    public static class NullableEqualityBuilder
    {
        public static FilterDefinition<TModel> ToDbFilter<TModel, TProperty>(
            this NullableEquality<TProperty> filterProperty,
            FilterDefinitionBuilder<TModel> filterBuilder,
            Expression<Func<TModel, TProperty>> modelProperty)
        {
            return BooleanFilterBuilder.ToDbFilter(
                filterProperty, 
                filterBuilder,
                __ => EqualWithValue(modelProperty, filterProperty, filterBuilder));
        }

        private static FilterDefinition<TModel> EqualWithValue<TModel, TProperty>(
            Expression<Func<TModel, TProperty>> modelProperty,
            NullableEquality<TProperty> filterProperty,
            FilterDefinitionBuilder<TModel> filterBuilder)
        {
            string propertyName = ((MemberExpression)modelProperty.Body).Member.Name;
            if (!filterProperty.HasValue)
            {
                return new JsonFilterDefinition<TModel>($"{{{propertyName} : null}}");
            }
            return filterBuilder.Eq(modelProperty, filterProperty.Value);
        }
    }
}
