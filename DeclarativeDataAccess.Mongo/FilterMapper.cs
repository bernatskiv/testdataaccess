﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeclarativeDataAccess.Mongo
{
    public class FilterMapper<TFilter, TModel>
    {
        /// <summary>
        /// FilterDefinition<TModel> - this is the type of DbFilter. 
        /// So all mappings goes from TFilter to this Expression type
        /// </summary>
        private List<Func<TFilter, FilterDefinition<TModel>>> mappers =
            new List<Func<TFilter, FilterDefinition<TModel>>>();

        private FilterDefinitionBuilder<TModel> dbFilterBuilder => Builders<TModel>.Filter;

        public FilterDefinition<TModel> Map(TFilter filter)
        {
            var dbFilters = mappers.Select(map => map(filter))
                                   .Where(dbFilter => dbFilter != null)
                                   .ToArray();
            return dbFilters.Any()
                ? dbFilterBuilder.And(dbFilters)
                : BuildEmptyFilter();
        }

        public FilterMapper<TFilter, TModel> AddMapping<TFilterProperty>(
            Func<TFilter, TFilterProperty> filterPropertySelector,
            Func<TFilterProperty, FilterDefinitionBuilder<TModel>, FilterDefinition<TModel>> propretyToDbFilterMapper)
        {
            if (filterPropertySelector == null) throw new ArgumentNullException(nameof(filterPropertySelector));
            if (propretyToDbFilterMapper == null) throw new ArgumentNullException(nameof(propretyToDbFilterMapper));
            mappers.Add(filter => MapProperty(filterPropertySelector, propretyToDbFilterMapper, filter));
            return this;
        }

        private FilterDefinition<TModel> MapProperty<TFilterProperty>(
            Func<TFilter, TFilterProperty> filterPropertySelector,
            Func<TFilterProperty, FilterDefinitionBuilder<TModel>, FilterDefinition<TModel>> propretyToFilterMapper,
            TFilter filter)
        {
            var filterProperty = filterPropertySelector(filter);
            return filterProperty != null ? propretyToFilterMapper(filterProperty, dbFilterBuilder) : null;
        }

        private FilterDefinition<TModel> BuildEmptyFilter()
        {
            return dbFilterBuilder.Empty;
        }
    }
}
