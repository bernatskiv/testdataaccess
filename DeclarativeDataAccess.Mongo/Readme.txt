﻿DeclarativeDataAccess.Mongo project contains FilterBuilders and FilterMapper.
FilterBuilders are classes that creates FilterDefinition for filtering of mongo collections. These FilterDefinitions are created basing on filter descriptions that in DeclarativeDataAccess project.
FilterMapper is used to map filter-object to FilterDefinition.