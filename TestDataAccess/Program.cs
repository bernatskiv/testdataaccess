﻿using TestDataAccess.DI;
using TestExample.Domain.Contract.Services;
using Microsoft.Extensions.DependencyInjection;
using TestExample.Domain.Contract.Entities;
using System;

namespace TestDataAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = DiConfigs.InitMongoConfig();
            using (var scope = container.CreateScope())
            {
                ///Пример чисто для изучения по коду. Пока ничего не раннается. Стоят заглушки. 
                ///Написаны интерфейсы и реализована бизнес логика. Репозиторий пока заглушка
                ///Вот ситуация, где у тебя прилага с контентом 18+

                var userService = scope.ServiceProvider.GetService<IUserService>();

                //userService.Create(new UserEntity()
                //{
                //    Id = 2,
                //    DateOrBirth = DateTime.Now.AddYears(-10),
                //    FirstName = "Jane",
                //    LastName = "dfs",
                //    Gender = Gender.Male,
                //    Height = 200
                //});

                ///Ну этот пример чисто показать удобность на простых примерах
                var tallMen = userService.GetTallMenWithName("john");

                ///К примеру ты хочешь разрешить доступ к 18+ контенту только взрослым
                var adultList = userService.GetAdultPeople();

                ///А для всех детей или лиц не указавших возраст ты хочешь отослать уведомления, 
                ///что контент 18+ не доступен им
                var nonAdultList = userService.GetNonAdult(true);

                var test1 = userService.GetAll();
                //var test = userService.GetById(2);
            }
        }
    }
}
