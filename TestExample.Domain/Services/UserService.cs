﻿using DeclarativeDataAccess.Contract.FilterModels;
using DeclarativeDataAccess.FilterModels;
using System;
using System.Collections.Generic;
using System.Text;
using TestExample.DataAccess.Contract.Repositories;
using TestExample.Domain.Contract.Entities;
using TestExample.Domain.Contract.Services;

namespace TestExample.Domain.Services
{
    public interface IDateProvider
    {
        DateTime GetCurrentDate();
    }

    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IDateProvider dateProvider;

        public UserService(IUserRepository userRepository, IDateProvider dateProvider)
        {
            this.userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            this.dateProvider = dateProvider ?? throw new ArgumentNullException(nameof(dateProvider));
        }

        public void Create(UserEntity entity) => userRepository.Create(entity);

        public UserEntity GetById(int id) => userRepository.GetById(id);

        public void RemoveById(int id) => userRepository.RemoveById(id);

        public void Update(UserEntity entity) => userRepository.Update(entity);

        public IEnumerable<UserEntity> GetAdultPeople()
        {
            var minDate = dateProvider.GetCurrentDate().AddYears(-18);

            return userRepository.GetByFilter(new UserFilter()
            {
                DateOrBirth = CompositeFilter<DateOfBirthFilter>.Or(
                    DateOfBirthFilter.InitInterval(ValueInInterval<DateTime>.LessThanOrEqual(minDate)))
            });
        }

        public IEnumerable<UserEntity> GetNonAdult(bool includeWithoutDob)
        {
            var dobFilter = new List<DateOfBirthFilter>();

            var minDate = dateProvider.GetCurrentDate().AddYears(-18);
            dobFilter.Add(DateOfBirthFilter.InitInterval(ValueInInterval<DateTime>.GreaterThan(minDate)));

            if (includeWithoutDob)
            {
                dobFilter.Add(DateOfBirthFilter.InitNullable(NullableEquality<DateTime?>.CreateNull()));
            }

            return userRepository.GetByFilter(new UserFilter()
            {
                DateOrBirth = CompositeFilter<DateOfBirthFilter>.Or(dobFilter.ToArray())
            });
        }

        public IEnumerable<UserEntity> GetTallMenWithName(string name)
        {
            return userRepository.GetByFilter(new UserFilter()
            {
                Height = ValueInInterval<int>.GreaterThan(190),
                FirstName = StringSearchParameter.SearchFor(name, StringSearchType.FullMatch),
                Gender = NonNullableEquality<Gender>.Equal(Gender.Male)
            });
        }

        public IEnumerable<UserEntity> GetNonMale()
        {
            var filter = new UserFilter()
            {
                Gender = BooleanFilter.Not(NonNullableEquality<Gender>.Equal(Gender.Male))
            };

            return userRepository.GetByFilter(filter);
        }

        public IEnumerable<UserEntity> GetAll()
        {
            return userRepository.GetByFilter(new UserFilter());
        }
    }
}
