﻿using System;
using System.Collections.Generic;
using System.Text;
using TestExample.Domain.Services;

namespace TestExample.Domain
{
    public class DateProvider : IDateProvider
    {
        public DateTime GetCurrentDate()
        {
            return DateTime.Now;
        }
    }
}
