﻿using DeclarativeDataAccess.FilterModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeclarativeDataAccess.Contract.FilterModels
{
    public sealed class NullableEquality<TProperty> : BooleanFilter
    {
        private readonly TProperty _value;
        #region Properties
        public TProperty Value => HasValue
            ? _value
            : throw new InvalidOperationException("Cannot read value that doesn't exist");
        public bool HasValue { get; }
        #endregion

        #region Initializers
        public static NullableEquality<TProperty> CreateNull()
        {
            return new NullableEquality<TProperty>();
        }

        public static NullableEquality<TProperty> CreateValue(TProperty value)
        {
            return new NullableEquality<TProperty>(value);
        }
        #endregion

        #region Private
        private NullableEquality(TProperty value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            _value = value;
            HasValue = true;
        }

        private NullableEquality()
        {
            HasValue = false;
        }
        #endregion
    }
}
