﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeclarativeDataAccess.FilterModels
{
    public abstract class BooleanFilter
    {
        public bool Inverted { get; private set; }

        public static TFilter Not<TFilter>(TFilter filter)
            where TFilter : BooleanFilter
        {
            filter.Inverted = !filter.Inverted;
            return filter;
        }
    }
}
