﻿using DeclarativeDataAccess.FilterModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeclarativeDataAccess.Contract.FilterModels
{
    public sealed class AnyOf<T> : BooleanFilter
    {
        public IReadOnlyList<T> Values { get; }

        public static AnyOf<T> Init(params T[] items)
        {
            return new AnyOf<T>(items);
        }

        #region Private
        private AnyOf(T[] items)
        {
            if (items == null || items.Any(item => item == null))
            {
                throw new ArgumentNullException(nameof(items));
            }

            Values = items;
        }
        #endregion
    }
}
