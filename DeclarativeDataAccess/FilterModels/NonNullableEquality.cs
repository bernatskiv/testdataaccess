﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeclarativeDataAccess.FilterModels
{
    public sealed class NonNullableEquality<TValue> : BooleanFilter
    {
        public TValue Value { get; }

        public static NonNullableEquality<TValue> Equal(TValue value) => new NonNullableEquality<TValue>(value);

        private NonNullableEquality(TValue value)
        {
            if(value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            Value = value;
        }
    }
}
