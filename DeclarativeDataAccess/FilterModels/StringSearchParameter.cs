﻿using DeclarativeDataAccess.FilterModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeclarativeDataAccess.Contract.FilterModels
{
    public sealed class StringSearchParameter : BooleanFilter
    {
        public string String { get; }
        public StringSearchType MatchType { get; }

        public static StringSearchParameter SearchFor(string @string, StringSearchType matchType)
        {
            return new StringSearchParameter(@string, matchType);
        }

        private StringSearchParameter(string @string, StringSearchType matchType)
        {
            String = @string ?? throw new ArgumentNullException(nameof(@string));
            MatchType = matchType;
        }
    }

    public enum StringSearchType
    {
        FullMatch = 0,
        SubStringMatch
    }
}
