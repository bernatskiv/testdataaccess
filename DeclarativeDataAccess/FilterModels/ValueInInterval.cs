﻿using DeclarativeDataAccess.FilterModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeclarativeDataAccess.Contract.FilterModels
{
    public sealed class ValueInInterval<TValue> : BooleanFilter
    {
        #region Propreties
        public IntervalEnding<TValue> Max { get; }
        public IntervalEnding<TValue> Min { get; }
        public bool IsExactValue { get; }
        #endregion

        #region Initializers
        public static ValueInInterval<TValue> CreateExactValue(TValue value)
        {
            return new ValueInInterval<TValue>(value);
        }

        public static ValueInInterval<T> CreateInterval<T>(IntervalEnding<T> min = null, IntervalEnding<T> max = null)
            //where T : IComparable<T>
        {
            //Func<T, T, int> compare = (left, right) => left.CompareTo(right);

            return ValueInInterval<T>.CreateInterval1(/*compare,*/ min, max);
        }

        public static ValueInInterval<TValue> CreateInterval1(
            //Func<TValue, TValue, int> compare,
            IntervalEnding<TValue> min = null,
            IntervalEnding<TValue> max = null)
        {
            return new ValueInInterval<TValue>(min, max/*, compare*/);
        }

        public static ValueInInterval<T> GreaterThan<T>(T value)// where T : IComparable<T>
            => CreateInterval<T>(min: IntervalEnding<T>.ExcludingValue(value));
        public static ValueInInterval<T> GreaterThanOrEqual<T>(T value)// where T : IComparable<T>
            => CreateInterval<T>(min: IntervalEnding<T>.IncludingValue(value));
        public static ValueInInterval<T> LessThan<T>(T value) //where T : IComparable<T>
            => CreateInterval<T>(max: IntervalEnding<T>.ExcludingValue(value));
        public static ValueInInterval<T> LessThanOrEqual<T>(T value) //where T : IComparable<T>
            => CreateInterval<T>(max: IntervalEnding<T>.IncludingValue(value));
        #endregion

        #region Private
        private ValueInInterval(TValue exactValue)
        {
            var valueOption = IntervalEnding<TValue>.IncludingValue(exactValue);
            Min = valueOption;
            Max = valueOption;
            IsExactValue = true;
        }

        private ValueInInterval(IntervalEnding<TValue> min, IntervalEnding<TValue> max/*, Func<TValue, TValue, int> compare*/)
        {
            min = ToNonNullable(min);
            max = ToNonNullable(max);

            //if (min.HasBorder && max.HasBorder)
            //{
            //    var compareResult = compare(max.Value, min.Value);
            //    if (compareResult < 0)
            //    {
            //        throw new ArgumentException("Max value cannot be less than min");
            //    }
            //    if (compareResult == 0)
            //    {
            //        IsExactValue = true;
            //    }
            //}
            IsExactValue = min == max;
            Min = min;
            Max = max;
        }

        private static IntervalEnding<TValue> ToNonNullable(IntervalEnding<TValue> value)
        {
            return value ?? IntervalEnding<TValue>.NoBorder();
        }
        #endregion
    }

    public sealed class IntervalEnding<TValue>
    {
        private readonly TValue _value;
        #region Properties
        public IntervalEndingType Type { get; }
        public TValue Value => HasBorder ? _value : throw new InvalidOperationException("Cannot get value");
        public bool HasBorder => Type != IntervalEndingType.NoBorder;
        #endregion

        #region Initializers
        public static IntervalEnding<TValue> IncludingValue(TValue value)
        {
            return new IntervalEnding<TValue>(NotNull(value), IntervalEndingType.Including);
        }

        public static IntervalEnding<TValue> ExcludingValue(TValue value)
        {
            return new IntervalEnding<TValue>(NotNull(value), IntervalEndingType.Excluding);
        }

        public static IntervalEnding<TValue> NoBorder()
        {
            return new IntervalEnding<TValue>();
        }
        #endregion

        #region Private
        private IntervalEnding(TValue value, IntervalEndingType type)
        {
            _value = value;
            Type = type;
        }

        private IntervalEnding()
        {
            Type = IntervalEndingType.NoBorder;
        }

        private static TValue NotNull(TValue value)
        {
            if (value == null)
            {
                throw new NullReferenceException(nameof(value));
            }

            return value;
        }
        #endregion
    }

    public enum IntervalEndingType
    {
        Including = 0,
        Excluding,
        NoBorder
    }
}
