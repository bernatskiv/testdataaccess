﻿using DeclarativeDataAccess.FilterModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeclarativeDataAccess.Contract.FilterModels
{
    public sealed class CompositeFilter<TFilter> : BooleanFilter
    {
        #region Properties
        public CompositionType Type { get; }
        public IReadOnlyList<TFilter> Filters { get; }
        public IReadOnlyList<CompositeFilter<TFilter>> CompositeFilters { get; }
        #endregion

        #region Initializers
        public static CompositeFilter<TFilter> And(
            TFilter[] filters = null,
            CompositeFilter<TFilter>[] compositeFilters = null)
            => new CompositeFilter<TFilter>(CompositionType.And, filters ?? emptyFilter, compositeFilters ?? emptyCompositeFilter);
        public static CompositeFilter<TFilter> And(params TFilter[] filters) => And(filters, null);
        public static CompositeFilter<TFilter> And(params CompositeFilter<TFilter>[] filters) => And(null, filters);

        public static CompositeFilter<TFilter> Or(
            TFilter[] filters = null,
            CompositeFilter<TFilter>[] compositeFilters = null)
            => new CompositeFilter<TFilter>(CompositionType.Or, filters ?? emptyFilter, compositeFilters ?? emptyCompositeFilter);
        public static CompositeFilter<TFilter> Or(params TFilter[] filters) => Or(filters, null);
        public static CompositeFilter<TFilter> Or(params CompositeFilter<TFilter>[] filters) => Or(null, filters);
        #endregion

        #region Private
        private CompositeFilter(CompositionType compositionType, TFilter[] filters, CompositeFilter<TFilter>[] compositeFilters)
        {
            Type = compositionType;
            Filters = filters ?? throw new ArgumentNullException(nameof(filters));
            CompositeFilters = compositeFilters ?? throw new ArgumentNullException(nameof(compositeFilters));
        }

        private static TFilter[] emptyFilter = new TFilter[0];
        private static CompositeFilter<TFilter>[] emptyCompositeFilter = new CompositeFilter<TFilter>[0];
        #endregion
    }

    public enum CompositionType
    {
        And = 0,
        Or
    }
}
