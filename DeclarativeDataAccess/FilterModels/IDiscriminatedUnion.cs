﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeclarativeDataAccess.Contract.FilterModels
{
    /// <summary>
    /// This interface is used to signal that corresponding class imitates discriminated unions
    /// It means that it contains set of properties and single property is not null
    /// In other words: this class can be in one of states
    /// </summary>
    public interface IDiscriminatedUnion
    {
    }
}
